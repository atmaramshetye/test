class Restaurant(object):
    def __init__(self, id):
        self.id = id
        self.offers = {}

    def __str__(self):
        return str(self.id)

    def add_offer(self, items, price):
        offer = (items, price)
        for item in items:
            if self.offers.has_key(item):
                self.offers[item].append(offer)
            else:
                self.offers[item] = [offer]
            
    def get_matching_offers(self, item):
        return self.offers.get(item, [])

    def can_cater(self, items):
        for item in items:
            if not self.get_matching_offers(item):
                return False
        return True

    def get_cheapest_offer(self, target_items):
        """
        Loops through all matching offers and explore a recursive tree
        search (brute force) to get the combination with the lowest price
        """
        def _remaining_items_(target_items, offer_items):
            remaining = []
            for i in set(target_items):
                diff = target_items.count(i) - offer_items.count(i)
                remaining.extend([i]*diff)
            return remaining

        def _cheapest_offer_(items, cost):
            cheapest_price = float("inf")
            for offer in self.offers.get(items[0], []):
                remaining = _remaining_items_(items, offer[0])
                if remaining:
                    curr_price = _cheapest_offer_(
                            remaining, cost + offer[1])
                else:
                    curr_price = cost + offer[1]
                if curr_price < cheapest_price:
                    cheapest_price = curr_price
            return cheapest_price

        if not target_items:
            return float("inf")
        return _cheapest_offer_(target_items, 0.0)

if __name__ == '__main__':
    r = Restaurant(1)
    r.add_offer(('a',), 20.0)
    r.add_offer(('a', 'b'), 30.0)
    r.add_offer(('a', 'b', 'c'), 35.0)
    r.add_offer(('b',), 15.0)
    r.add_offer(('c',), 10.0)
    
    items = ('a', 'a', 'b')
    assert r.can_cater(items)
    assert r.get_cheapest_offer(items) == 50.0

    r = Restaurant(2)
    assert r.can_cater(items) == False
    print r.get_cheapest_offer(items)
