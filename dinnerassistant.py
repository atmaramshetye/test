from restaurant import Restaurant
import sys

class DinnerAssistant(object):
    def __init__(self, menu_csv_file=None):
        self.restaurants = {}
        if menu_csv_file:
            try:
                with file(menu_csv_file) as f:
                    self.parse(f)
            except IOError:
                # TODO: Log the exception
                print "Error opening file", csv_file

    def parse(self, filehandler):
        """
        Parse the csv input and store the menu data with each restaurant
        """
        self.restaurants.clear()
        for line in filehandler:
            try:
                menu_data = line.split(",")
                rest_id, price = int(menu_data[0].strip()), float(menu_data[1])
                items = tuple([i.strip().lower() for i in menu_data[2:]])
            except:
                # TODO: Log the exception
                continue

            if not self.restaurants.has_key(rest_id):
                self.restaurants[rest_id] = Restaurant(rest_id)

            self.restaurants[rest_id].add_offer(items, price)
                
    def get_candidate_restaurants(self, items):
        return [r for r in self.restaurants.values() if r.can_cater(items)]
            
    def suggest(self, items):
        """
        Loop through all restaurants which serve all items from the 
        choice list and return the one with the minimum price
        """
        candidates = self.get_candidate_restaurants(items)
        restaurant, cheapest_price = None, float("inf")
        for c in candidates:
            offer_price = c.get_cheapest_offer(items)
            if offer_price < cheapest_price:
                cheapest_price = offer_price
                restaurant = c
        return restaurant, cheapest_price

def test():
    import cStringIO
    da = DinnerAssistant()
    filehandler = cStringIO.StringIO("""5, 4.00, extreme_fajita
            5, 8.00, fancy_european_water
            6, 5.00, fancy_european_water
            6, 6.00, extreme_fajita, jalapeno_poppers, extra_salsa""")
    da.parse(filehandler)
    rest, price = da.suggest(('fancy_european_water', 'extreme_fajita'))
    assert (rest.id, price) == (6, 11.0)

def run():
    da = DinnerAssistant(sys.argv[1])
    best_restaurant, best_price = da.suggest(sys.argv[2:])
    if best_restaurant:
        print "%s, %.02f" % (best_restaurant, best_price)
    else:
        print "Sorry, none of the restaurants can cater to you :("

if __name__ == '__main__':
    #test()
    if len(sys.argv) < 3:
        print "Usage: python %s MENU_CSV_FILE ITEM1 [ITEM2 [ITEM3....]]" % sys.argv[0]
        sys.exit(1)
    run()
