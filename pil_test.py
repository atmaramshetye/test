import Image
import sys

if len(sys.argv) != 4:
    print "Usage: %s infile1 infile2 outfile" % sys.argv[0]

infile1 = sys.argv[1]
infile2 = sys.argv[2]
outfile = sys.argv[3]

im1 = Image.open(infile1)
im2 = Image.open(infile2)

region = im1.crop((0, 0, 25, 25))
im2.paste(region, (0, 0, 25, 25))
im2.save(outfile, "GIF")
