from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required

@login_required
def home(request):
    context = {
        'username' : request.user.username,
    }
    return render_to_response("home.html", context, context_instance=RequestContext(request))
