from django.contrib.auth.models import User
from django.db import models

class Manager(models.Model):
    user = models.OneToOneField(User)

    def __unicode__(self):
        return self.user.username

class Project(models.Model):
    name = models.CharField(max_length=100)
    manager = models.ForeignKey(Manager)

    def __unicode__(self):
        return self.name

class Role(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    manager = models.ForeignKey(Manager)
    # Assuming one employee can be assigned to only one project
    project = models.ForeignKey(Project)
    role = models.ForeignKey(Role)

    def __unicode__(self):
        return self.user.username
