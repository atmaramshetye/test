from django.contrib import admin
from users.models import UserProfile, Manager, Project, Role

admin.site.register(UserProfile)
admin.site.register(Manager)
admin.site.register(Role)
admin.site.register(Project)
