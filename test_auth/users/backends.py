from django.contrib.auth.models import User
import ldap
from ldap_settings import ldap_handle
import logging

logger = logging.getLogger("ldap")

class LDAPBackend:

    supports_object_permissions = False
    supports_anonymous_user = False
    supports_inactive_user = False

    def authenticate(self, username=None, password = None):
        try:
            ldap_handle.simple_bind_s(username, password)
        except ldap.INVALID_CREDENTIALS:
            return None
        except Exception, e:
            logger.error(str(e))
            return None

        return User.objects.get_or_create(username__iexact=username, defaults={'username': username.lower()})[0]

    def get_user(self, user_id):
        return User.objects.get(id = user_id)
