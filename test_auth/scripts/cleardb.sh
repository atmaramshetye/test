#!/bin/bash

sudo -u postgres psql < reinitializedb.sql
cd ..
sudo -u synerzip python manage.py syncdb < scripts/syncdb_input
cd -
